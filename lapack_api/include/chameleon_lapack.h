/**
 *
 * @file chameleon_lapack.h
 *
 * @copyright 2022-2025 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 ***
 *
 * @brief Chameleon blas/lapack and cblas/lapack api functions
 *
 * @version 1.3.0
 * @author Mathieu Faverge
 * @author Florent Pruvost
 * @date 2024-02-18
 *
 */
#ifndef _chameleon_lapack_h_
#define _chameleon_lapack_h_

#include "chameleon/chameleon_slapack.h"
#include "chameleon/chameleon_dlapack.h"
#include "chameleon/chameleon_clapack.h"
#include "chameleon/chameleon_zlapack.h"

#endif /* _chameleon_lapack_h_ */
